import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home/Home.vue';
import Dashboard from './views/Dashboard/Dashboard.vue';
import Contract from './views/Dashboard/Contract.vue';
import MyClients from './views/Dashboard/MyClients.vue';
import Propertys from './views/Dashboard/Propertys.vue';
import Vuelidade from 'vuelidate';

Vue.use(Router);
Vue.use(Vuelidade)

export default new Router({
    mode: "history",
    routes: [{
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/dashboard/painel',
            name: 'dashboard',
            component: Dashboard,
        },
        {
            path: '/dashboard/contract',
            name: 'dashboard',
            component: Contract,
        },
        {
            path: '/dashboard/my-clients',
            name: 'dashboard',
            component: MyClients,
        },
        {
            path: '/dashboard/property',
            name: 'dashboard',
            component: Propertys,
        },
    ]
})